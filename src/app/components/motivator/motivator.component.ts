import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-motivator',
  templateUrl: './motivator.component.html',
  styleUrls: ['./motivator.component.scss'],
})
export class MotivatorComponent implements OnInit {
  public currentQuote = '';
  private quotes: string[] = [];

  constructor() {}

  ngOnInit() {
    this.quotes = [
      'The worst thing I can be is the same as everybody else. - Arnold Schwarzenegger',
      'Strength does not come from winning. Your struggles develop your strengths. - Arnold Schwarzenegger',
      'The pessimist sees difficulty in every opportunity. The optimist sees the opportunity in every difficulty. - Winston Churchill ',
      'Creativity is intelligence having fun. - Albert Einstein',
      'Do what you can, with all you have, wherever you are. - Theodore Roosevelt',
    ];

    this.setRandomQuote();
  }

  setRandomQuote() {
    const randomIndex = Math.floor(Math.random() * this.quotes.length);
    this.currentQuote = this.quotes[randomIndex];
  }
}
